

# Oppretter alle våres OU
New-ADOrganizationalUnit 'AllUsers' -Description 'Contains OUs and users'
New-ADOrganizationalUnit 'IT' -Description 'IT staff' `
  -Path 'OU=AllUsers,DC=hjelmaas,DC=gruppen'
New-ADOrganizationalUnit 'Oko' -Description 'Economy Department' `
  -Path 'OU=AllUsers,DC=hjelmaas,DC=gruppen'
New-ADOrganizationalUnit 'Mark' -Description 'Markedsforing' `
  -Path 'OU=AllUsers,DC=hjelmaas,DC=gruppen'
New-ADOrganizationalUnit 'Sikk' -Description 'Sikkerhets IT' `
  -Path 'OU=IT,OU=AllUsers,DC=hjelmaas,DC=gruppen'
New-ADOrganizationalUnit 'Dev' -Description 'Utviklere' `
  -Path 'OU=IT,OU=AllUsers,DC=hjelmaas,DC=gruppen'
New-ADOrganizationalUnit 'Blue' -Description 'Blue Team' `
  -Path 'OU=Sikk,OU=IT,OU=AllUsers,DC=hjelmaas,DC=gruppen'
New-ADOrganizationalUnit 'Red' -Description 'Red Team' `
  -Path 'OU=Sikk,OU=IT,OU=AllUsers,DC=hjelmaas,DC=gruppen'
New-ADOrganizationalUnit 'HR' `
  -Description 'Human Resources' `
  -Path 'OU=AllUsers,DC=hjelmaas,DC=gruppen'

# Computer OUer
New-ADOrganizationalUnit 'Klienter' `
  -Description 'Contains OUs and users laptops'
New-ADOrganizationalUnit 'Server' `
  -Description 'Contains OUs and servers'
New-ADOrganizationalUnit 'Adm' -Description 'Adm laptops' `
  -Path 'OU=Klienter,DC=hjelmaas,DC=gruppen'
New-ADOrganizationalUnit 'Dev' -Description 'Utviklere laptops' `
  -Path 'OU=Klienter,DC=hjelmaas,DC=gruppen'  
New-ADOrganizationalUnit 'Server' -Description 'Servere' `
  -Path 'OU=Server,DC=hjelmaas,DC=gruppen'  

#Flytter alle Computerne til riktig sted
Get-ADComputer "MGR" | 
  Move-ADObject -TargetPath "OU=Adm,OU=Klienter,DC=hjelmaas,DC=gruppen"
Get-ADComputer "CL1" | 
  Move-ADObject -TargetPath "OU=Dev,OU=Klienter,DC=hjelmaas,DC=gruppen"
Get-ADComputer "SRV1" | 
  Move-ADObject -TargetPath "OU=Server,DC=hjelmaas,DC=gruppen"