# Lager grupper for alle OU
$GROUP = @{
  Name          = "g_Hjelmaas RDP Brukere"
  GroupCategory = "Security"
  GroupScope    = "Global"
  DisplayName   = "Local RDP users"
  Path          = "OU=AllUsers,DC=hjelmaas,DC=gruppen"
  Description   = "Users/Groups that will be allowed to RDP to CL1"
}
New-ADGroup @GROUP


$GROUP = @{
  Name          = "g_IT"
  GroupCategory = "Security"
  GroupScope    = "Global"
  DisplayName   = "Local IT"
  Path          = "OU=IT,OU=AllUsers,DC=hjelmaas,DC=gruppen"
  Description   = "Gruppe for IT"
}
New-ADGroup @GROUP

$GROUP = @{
  Name          = "g_Dev"
  GroupCategory = "Security"
  GroupScope    = "Global"
  DisplayName   = "Local Dev"
  Path          = "OU=Dev,OU=IT,OU=AllUsers,DC=hjelmaas,DC=gruppen"
  Description   = "Gruppe for utviklere"
}
New-ADGroup @GROUP

$GROUP = @{
  Name          = "g_Sikk"
  GroupCategory = "Security"
  GroupScope    = "Global"
  DisplayName   = "Local Sikkerhet"
  Path          = "OU=Sikk,OU=IT,OU=AllUsers,DC=hjelmaas,DC=gruppen"
  Description   = "Gruppe for sikkerhet"
}
New-ADGroup @GROUP

$GROUP = @{
  Name          = "g_Blue"
  GroupCategory = "Security"
  GroupScope    = "Global"
  DisplayName   = "Local Sikkerhet Blue"
  Path          = "OU=Blue,OU=Sikk,OU=IT,OU=AllUsers,DC=hjelmaas,DC=gruppen"
  Description   = "Gruppe for Sikkerhet Blue"
}
New-ADGroup @GROUP

$GROUP = @{
  Name          = "g_Red"
  GroupCategory = "Security"
  GroupScope    = "Global"
  DisplayName   = "Local Sikkerhet Red"
  Path          = "OU=Red,OU=Sikk,OU=IT,OU=AllUsers,DC=hjelmaas,DC=gruppen"
  Description   = "Gruppe for Sikkerhet Red"
}
New-ADGroup @GROUP

$GROUP = @{
  Name          = "g_Oko"
  GroupCategory = "Security"
  GroupScope    = "Global"
  DisplayName   = "Local Okonomi"
  Path          = "OU=Oko,OU=AllUsers,DC=hjelmaas,DC=gruppen"
  Description   = "Gruppe for Okonomi"
}
New-ADGroup @GROUP

$GROUP = @{
  Name          = "g_Mark"
  GroupCategory = "Security"
  GroupScope    = "Global"
  DisplayName   = "Local Markedsforing"
  Path          = "OU=Mark,OU=AllUsers,DC=hjelmaas,DC=gruppen"
  Description   = "Gruppe for Markedsforing"
}
New-ADGroup @GROUP

$GROUP = @{
  Name          = "g_HR"
  GroupCategory = "Security"
  GroupScope    = "Global"
  DisplayName   = "Local HR"
  Path          = "OU=HR,OU=AllUsers,DC=hjelmaas,DC=gruppen"
  Description   = "Gruppe for HR"
}
New-ADGroup @GROUP



#Denne koden går gjennom alle Users i hver spesifisert OU og legger disse til i en spesifisert gruppe.
#
Get-ADUser -Filter * -SearchBase "OU=Dev,OU=IT,OU=AllUsers,DC=hjelmaas,DC=gruppen" | ForEach-Object {Add-ADGroupMember -Identity 'g_Dev' -Members $_.sAMAccountName}
Get-ADUser -Filter * -SearchBase "OU=Blue,OU=Sikk,OU=IT,OU=AllUsers,DC=hjelmaas,DC=gruppen" | ForEach-Object {Add-ADGroupMember -Identity 'g_Blue' -Members $_.sAMAccountName}
Get-ADUser -Filter * -SearchBase "OU=Red,OU=Sikk,OU=IT,OU=AllUsers,DC=hjelmaas,DC=gruppen" | ForEach-Object {Add-ADGroupMember -Identity 'g_Red' -Members $_.sAMAccountName}
Get-ADUser -Filter * -SearchBase "OU=Mark,OU=AllUsers,DC=hjelmaas,DC=gruppen" | ForEach-Object {Add-ADGroupMember -Identity 'g_Mark' -Members $_.sAMAccountName}
Get-ADUser -Filter * -SearchBase "OU=Oko,OU=AllUsers,DC=hjelmaas,DC=gruppen" | ForEach-Object {Add-ADGroupMember -Identity 'g_Oko' -Members $_.sAMAccountName}
Get-ADUser -Filter * -SearchBase "OU=HR,OU=AllUsers,DC=hjelmaas,DC=gruppen" | ForEach-Object {Add-ADGroupMember -Identity 'g_HR' -Members $_.sAMAccountName}




#Gir en gruppe tilgang til remote desktop
#Dette må gjøres inne på den 'PCen' som man skal gi tilgang til

#
#Add-LocalGroupMember -Group "Remote Desktop Users" `
#  -Member 'hjelmaas.gruppen\g_Dev'
#Get-LocalGroupMember -Group "Remote Desktop Users"
#
#Add-LocalGroupMember -Group "Remote Desktop Users" `
#  -Member 'hjelmaas.gruppen\g_Blue'
#Get-LocalGroupMember -Group "Remote Desktop Users"
#
#Add-LocalGroupMember -Group "Remote Desktop Users" `
#  -Member 'hjelmaas.gruppen\g_Red'
#Get-LocalGroupMember -Group "Remote Desktop Users"