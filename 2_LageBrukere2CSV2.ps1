if ((Get-ChildItem -ErrorAction SilentlyContinue hjelmaasBrukere.csv).Exists)
  {"You alread have the file hjelmaasBrukere.csv!"; return;}
if ($PSVersionTable.PSVersion.Major -eq 5)
  {Write-Output "This script cannot be executed in Windows PowerShell, please use PowerShell core"; return;}

# 100 unique firstnames without norwegian characters ('øæå')
#
$FirstName = @("Nora","Emma","Ella","Maja","Olivia","Emilie","Sofie","Leah",
               "Sofia","Ingrid","Frida","Sara","Tiril","Selma","Ada","Hedda",
               "Amalie","Anna","Alma","Eva","Mia","Thea","Live","Ida","Astrid",
               "Ellinor","Vilde","Linnea","Iben","Aurora","Mathilde","Jenny",
               "Tuva","Julie","Oda","Sigrid","Amanda","Lilly","Hedvig",
               "Victoria","Amelia","Josefine","Agnes","Solveig","Saga","Marie",
               "Eline","Oline","Maria","Hege","Jakob","Emil","Noah","Oliver",
               "Filip","William","Lucas","Liam","Henrik","Oskar","Aksel",
               "Theodor","Elias","Kasper","Magnus","Johannes","Isak","Mathias",
               "Tobias","Olav","Sander","Haakon","Jonas","Ludvig","Benjamin",
               "Matheo","Alfred","Alexander","Victor","Markus","Theo",
               "Mohammad","Herman","Adam","Ulrik","Iver","Sebastian","Johan",
               "Odin","Leon","Nikolai","Even","Leo","Kristian","Mikkel",
               "Gustav","Felix","Sverre","Adrian","Lars"
              )

# 100 unique lastnames
#
$LastName = @("Hansen","Johansen","Olsen","Larsen","Andersen","Pedersen",
              "Nilsen","Kristiansen","Jensen","Karlsen","Johnsen","Pettersen",
              "Eriksen","Berg","Haugen","Hagen","Johannessen","Andreassen",
              "Jacobsen","Dahl","Jørgensen","Henriksen","Lund","Halvorsen",
              "Sørensen","Jakobsen","Moen","Gundersen","Iversen","Strand",
              "Solberg","Svendsen","Eide","Knutsen","Martinsen","Paulsen",
              "Bakken","Kristoffersen","Mathisen","Lie","Amundsen","Nguyen",
              "Rasmussen","Ali","Lunde","Solheim","Berge","Moe","Nygård",
              "Bakke","Kristensen","Fredriksen","Holm","Lien","Hauge",
              "Christensen","Andresen","Nielsen","Knudsen","Evensen","Sæther",
              "Aas","Myhre","Hanssen","Ahmed","Haugland","Thomassen",
              "Sivertsen","Simonsen","Danielsen","Berntsen","Sandvik",
              "Rønning","Arnesen","Antonsen","Næss","Vik","Haug","Ellingsen",
              "Thorsen","Edvardsen","Birkeland","Isaksen","Gulbrandsen","Ruud",
              "Aasen","Strøm","Myklebust","Tangen","Ødegård","Eliassen",
              "Helland","Bøe","Jenssen","Aune","Mikkelsen","Tveit","Brekke",
              "Abrahamsen","Madsen"
             )

# Av 60 totalt folk, sa er 10 i markedsføring, 5 i økonomi, 40 i it 50/50 split, og 5 HR. Red team er 10, blue team er 10.
#
$OrgUnits = @("ou=IT,ou=AllUsers","ou=IT,ou=AllUsers",
              "ou=Mark,ou=AllUsers","ou=Mark,ou=AllUsers","ou=Mark,ou=AllUsers",
              "ou=Mark,ou=AllUsers","ou=Mark,ou=AllUsers","ou=Mark,ou=AllUsers",
              "ou=Mark,ou=AllUsers","ou=Mark,ou=AllUsers","ou=Mark,ou=AllUsers",
	      "ou=Mark,ou=AllUsers",
              "ou=Blue,ou=Sikk,ou=IT,ou=AllUsers","ou=Blue,ou=Sikk,ou=IT,ou=AllUsers",
	      "ou=Blue,ou=Sikk,ou=IT,ou=AllUsers","ou=Blue,ou=Sikk,ou=IT,ou=AllUsers",
	      "ou=Blue,ou=Sikk,ou=IT,ou=AllUsers","ou=Blue,ou=Sikk,ou=IT,ou=AllUsers",
	      "ou=Blue,ou=Sikk,ou=IT,ou=AllUsers","ou=Blue,ou=Sikk,ou=IT,ou=AllUsers",
	      "ou=Blue,ou=Sikk,ou=IT,ou=AllUsers","ou=Blue,ou=Sikk,ou=IT,ou=AllUsers",
              "ou=Red,ou=Sikk,ou=IT,ou=AllUsers","ou=Red,ou=Sikk,ou=IT,ou=AllUsers",
              "ou=Red,ou=Sikk,ou=IT,ou=AllUsers","ou=Red,ou=Sikk,ou=IT,ou=AllUsers",
              "ou=Red,ou=Sikk,ou=IT,ou=AllUsers","ou=Red,ou=Sikk,ou=IT,ou=AllUsers",
              "ou=Red,ou=Sikk,ou=IT,ou=AllUsers","ou=Red,ou=Sikk,ou=IT,ou=AllUsers",
              "ou=Red,ou=Sikk,ou=IT,ou=AllUsers","ou=Red,ou=Sikk,ou=IT,ou=AllUsers",
              "ou=Dev,ou=IT,ou=AllUsers","ou=Dev,ou=IT,ou=AllUsers",
              "ou=Dev,ou=IT,ou=AllUsers","ou=Dev,ou=IT,ou=AllUsers",
              "ou=Dev,ou=IT,ou=AllUsers","ou=Dev,ou=IT,ou=AllUsers",
              "ou=Dev,ou=IT,ou=AllUsers","ou=Dev,ou=IT,ou=AllUsers",
              "ou=Dev,ou=IT,ou=AllUsers","ou=Dev,ou=IT,ou=AllUsers",
              "ou=Dev,ou=IT,ou=AllUsers","ou=Dev,ou=IT,ou=AllUsers",
              "ou=Dev,ou=IT,ou=AllUsers","ou=Dev,ou=IT,ou=AllUsers",
              "ou=Dev,ou=IT,ou=AllUsers","ou=Dev,ou=IT,ou=AllUsers",
              "ou=Dev,ou=IT,ou=AllUsers","ou=Dev,ou=IT,ou=AllUsers",
              "ou=Dev,ou=IT,ou=AllUsers","ou=Dev,ou=IT,ou=AllUsers",
              "ou=Oko,ou=AllUsers","ou=Oko,ou=AllUsers",
              "ou=Oko,ou=AllUsers","ou=Oko,ou=AllUsers",
              "ou=Oko,ou=AllUsers",
              "ou=HR,ou=AllUsers","ou=HR,ou=AllUsers",
              "ou=HR,ou=AllUsers","ou=HR,ou=AllUsers",
              "ou=HR,ou=AllUsers"
             )

# Three shuffled indices to randomly mix firstname, lastname, and department
#
$fnidx = 0..99 | Get-Random -Shuffle
$lnidx = 0..99 | Get-Random -Shuffle
$ouidx = 0..99 | Get-Random -Shuffle

Write-Output "UserName;GivenName;SurName;UserPrincipalName;DisplayName;Password;Department;Path" > hjelmaasBrukere.csv

foreach ($i in 0..99) {
  $UserName          = $FirstName[$fnidx[$i]].ToLower()
  $GivenName         = $FirstName[$fnidx[$i]]
  $SurName           = $LastName[$lnidx[$i]]
  $UserPrincipalName = $UserName + '@' + 'hjelmaas.gruppen'
  $DisplayName       = $GivenName + ' ' + $SurName
  $Password          = -join ('abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPRSTUVWXYZ0123456789!"#$%&()*+,-./:<=>?@[\]_{|}'.ToCharArray() | Get-Random -Count 16) + '1.aA'
  $Department        = ($OrgUnits[$ouidx[$i]] -split '[=,]')[1]
  $Path              = $OrgUnits[$ouidx[$i]] + ',' + "dc=hjelmaas,dc=gruppen"
  Write-Output "$UserName;$GivenName;$SurName;$UserPrincipalName;$DisplayName;$Password;$Department;$Path" >> hjelmaasBrukere.csv
}



$ADUsers = Import-Csv hjelmaasBrukere.csv -Delimiter ';'
foreach ($User in $ADUsers) {
  if (!(Get-ADUser -LDAPFilter `
      "(sAMAccountName=$($User.Username))")) {
    New-ADUser `
    -SamAccountName        $User.Username `
    -UserPrincipalName     $User.UserPrincipalName `
    -Name                  $User.DisplayName `
    -GivenName             $User.GivenName `
    -Surname               $User.SurName `
    -Enabled               $True `
    -ChangePasswordAtLogon $False `
    -DisplayName           $user.Displayname `
    -Department            $user.Department `
    -Path                  $user.path `
    -AccountPassword `
    (ConvertTo-SecureString $user.Password -AsPlainText -Force)
  }
}
